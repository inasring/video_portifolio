Visualization of the output of the UNET during optimization (Paper deep image prior). Each frame the output after 50 optimization steps. After every 10 optimization steps some noise was added to the input image trying to reduce some of the dark spots.
This video was generated for the library in-painting example.

https://gitlab.com/inasring/projeto_final/-/blob/master/bandeira_trabalho.py


https://dmitryulyanov.github.io/deep_image_prior
https://sites.skoltech.ru/app/data/uploads/sites/25/2018/04/deep_image_prior.pdf
