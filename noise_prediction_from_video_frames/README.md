Visualization from the paper "Noise intensity prediction from video frames using deep convolutional neural networks"
https://pub.dega-akustik.de/ICA2019/data/articles/000869.pdf

Buses pass at 1:48 and at 2:10. The stronger the green color, the higher the predicted sound pressure is for that pixel. At those times, the buses are clearly highlighted.

Image in middle is the class activation map (CAM) of the network fine tuned to predict sound pressure intensity 
Image in the right has the red and blue channels of the original video and its green channel swapped for the filtered CAM. The filter is the difference between an the original CAM and a moving average of the CAM. This was done to avoid "stuck" pixels in the CAM and leave mostly pixels from moving objects in the image. This visualization helps checking that the network is associating objects in the image with sound pressure intensities.

git@gitlab.com:inasring/noise_img_v2.git