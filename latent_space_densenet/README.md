3d PCA of the output of the latent space of DenseNet (right before the fully connected layers)

It shows that even compressing the output of the final convolutional layer of a trained DenseNet model (448 parameters) to 3 dimensions the classes are still separated. 
Also, the distance in space of similar classes are shown to be really close to each other in the output space (The class clouds of automobiles and trucks / cats and dogs / horse and deers ).


dense_net_blocks > fully connected layers > classification


dense_net_blocks > latent tensor > flatten to vector > pca3d > plot
